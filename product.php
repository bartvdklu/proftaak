<!DOCTYPE html>
<html><!DOCTYPE html>
<html>

<head>
     <title>EduPlay</title>
     <link rel="shortcut icon" type="image/png" href="inc/img/favicon.png">
     <link href="inc/css/style.css" type="text/css" rel="stylesheet">
     <link href="inc/css/navbar.css" type="text/css" rel="stylesheet">
     <link href="inc/css/footer.css" type="text/css" rel="stylesheet">
</head>

<body>
     <?php include 'navbar.php';?>

     <div class = "productkop">
          <h1> ONS PRODUCT </h1>
          <p>Ons product probeert op spelenderwijze kinderen lesstof te leren in de vorm van een spel. Kinderen die het spel spelen zullen op interactieve wijze de vragen van een quiz beantwoorden waarbij ze een competitie tegen elkaar spelen. Dit allemaal in de vorm van bekende spellen die elk kind zo onder de knie heeft!</p>
     </div>

     <div class = "container">
          <div class = "productimage productright">
               <img src ="inc/img/quizdesign.png" alt="quiz">
          </div>

          <div class = "producttekst productleft">
               <h2> DE QUIZ </h2>
               <p>De quiz zal vragen bevatten die inhoudelijk zijn afgestemd op lesstof van verschillende vakken. Om de kennis van de kinderen te testen, laat je ze in een competitie zien wat ze wel en niet weten. Beide kinderen zijn zo gemotiveerd om de lesstof in zich op te nemen en eventueel te reflecteren op hun antwoorden.</p>
          </div>
     </div>

     <div class = "container">
          <div class = "productimage productleft">
               <img src ="inc/img/touwtrekken.jpg" alt="quiz">
          </div>

          <div class = "producttekst productright">
               <h2> TOUWTREKKEN </h2>
               <p>Elk kind kent het wel: touwtrekken! Indien een kind het juiste antwoord heeft gegeven is dat niet het enige wat er gebeurd. Als het spel start wordt een mobiel autootje op de middellijn tussen de spelers in geplaatst. Indien er juist geantwoord wordt zal de speler met die kleur dat meteen zien door dat de auto zijn richting op rijdt. Je kunt dus meteen zien wie er gaat winnen!</p>
          </div>
     </div>

     <div class = "container">
          <div class = "productimage productright">
               <img src ="inc/img/joystick.png" alt="quiz">
          </div>

          <div class = "producttekst productleft">
               <h2> RACE & JOYSTICK </h2>
               <p>De spelers krijgen elk hun eigen controller. Kinderen hebben graag interactie met het spel. De controller zorgt ervoor dat er een gevoel is dat ze het spel zelf in de hand hebben. De snelheid en het juiste antwoord bepaald hoe ver jij gaat komen in deze race.</p>
          </div>
     </div>

     <div class = "container">
          <div class = "productimage productleft">
               <img src ="inc/img/auto.png" alt="quiz">
          </div>

          <div class = "producttekst productright">
               <h2> DE ROBOT </h2>
               <p>De robot moet wat extra druk leggen op de spelers. Fysiek reflecteert de robot jouw prestaties. Dit geeft extra motivatie om de competitie aan te willen gaan en te winnen van je tegenspeler.  De interactie die het kind heeft met de quiz en de auto zorgt voor een interactieve en leerzame ervaring.</p>
          </div>
     </div>
     <?php include 'footer.php';?>
</body>
