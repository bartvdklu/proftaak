<!DOCTYPE html>
<html>

<head>
     <title>EduPlay</title>
     <link rel="shortcut icon" type="image/png" href="inc/img/favicon.png">
     <link href="inc/css/style.css" type="text/css" rel="stylesheet">
     <link href="inc/css/navbar.css" type="text/css" rel="stylesheet">
     <link href="inc/css/footer.css" type="text/css" rel="stylesheet">
</head>

<body>
     <?php include 'navbar.php';?>

     <h1>Wie zijn wij?</h1>

     <div class = "infotekst">
          <p>Wij zijn EduPlay, een game bedrijf gestart door 6 Fontys Hogeschool studenten in September 2017. In Oktober gaan we ons eerste
               product op de markt brengen genaamd Tug of School.</p>
          </div>

          <div class = "container">
               <div class = "productimage productright">
                    <img src ="inc/img/elearning.jpg" alt="quiz">
               </div>

               <div class = "producttekst productleft">
                    <h2>Wat kun je van ons verwachten?</h2>
                    <p>Waar wij ons de komende tijd mee bezig gaan houden is het op de markt brengen van een educatief spel dat door middel van E-learning het motivatieprobleem voor kinderen op school gaat aanpakken. Dit product gaan we de komende tijd ontwikkelen, testen en optimaliseren zodat we dit probleem ook daadwerkelijk aan kunnen pakken. Voor meer informatie over ons product, klik hier beneden.</p>
               </div>
          </div>

          <div class = "button">
              <a href="product.php"> TUG OF SCHOOL ></a>
         </div>

          <div class = "container">
               <div class = "productimage productleft">
                    <img src ="inc/img/zininschool.jpg" alt="quiz">
               </div>

               <div class = "producttekst productright">
                    <h2>Wat willen wij bereiken?</h2>
                    <p>Wij gaan zorgen voor een systeem dat het leren innovatiever, interactiever en leuker gaat maken door middel van E-learning en gamification, dit houdt in dat wij kinderen willen laten leren door middel van spelvorm en competitie. Niet meer uren lang in een boek lezen met uiteindelijk een toets, maar tegen je klasgenoot een quiz spelen met vragen over leerstof en actuele onderwerpen. Kinderen gaan met meer plezier en energie naar school.</p>
               </div>
          </div>
     <?php include 'footer.php';?>
</body>
</html>
