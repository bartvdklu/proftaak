<!DOCTYPE html>
<html>

<head>
     <title>EduPlay</title>
     <link rel="shortcut icon" type="image/png" href="inc/img/favicon.png">
     <link href="inc/css/style.css" type="text/css" rel="stylesheet">
     <link href="inc/css/navbar.css" type="text/css" rel="stylesheet">
     <link href="inc/css/footer.css" type="text/css" rel="stylesheet">
</head>

<body>
     <?php include 'navbar.php';?>
     <main>
          <video id = "video" src="inc/vid/vid.mp4" autoplay="true" loop="true" muted="true"></video>
     </main>
     <?php include 'footer.php';?>
</body>
</html>
