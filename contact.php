<!DOCTYPE html>
<html>

<head>
     <title>EduPlay</title>
     <link rel="shortcut icon" type="image/png" href="inc/img/favicon.png">
     <link href="inc/css/style.css" type="text/css" rel="stylesheet">
     <link href="inc/css/navbar.css" type="text/css" rel="stylesheet">
     <link href="inc/css/footer.css" type="text/css" rel="stylesheet">
</head>

<body>
     <?php include 'navbar.php';?>

     <div id="container">
      <form action="mailto:justinberghem@gmail.com" method="post" enctype="text/plain">

        <h3><label for="fname">First Name:</label></h3>
        <input type="text" id="fname" name="First name " placeholder="Your name..">

        <h3><label for="lname">Last Name:</label></h3>
        <input type="text" id="lname" name="Last name " placeholder="Your last name..">

        <h3><label for="country">Country:</label></h3>
        <select id="country" name="Country ">
          <option value="The Netherlands">The Netherlands</option>
          <option value="Belgium">Belgium</option>
        </select>

        <h3><label for="subject">Message:</label></h3>
        <textarea id="subject" name="Message " placeholder="Write something.." style="height:200px"></textarea>
        <input type="Submit" value="Send">

      </form>
    </div>

    <?php include 'footer.php';?>
</body>

</html>
